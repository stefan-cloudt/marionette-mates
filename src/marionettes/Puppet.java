package marionettes;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Puppet {
	String charName; // Character Name

	// Images
	// BufferedImage bodyRight;
	// BufferedImage bodyLeft;

	int jumpDist, moveSpeed, speakMax, eyebrowMax, tiltMax;
	boolean deadbonesStyle;
	ArrayList<BufferedImage> hatsRight = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> hatsLeft = new ArrayList<BufferedImage>();
	ArrayList<Integer> hatsList = new ArrayList<Integer>();
	ArrayList<BufferedImage> mouthRight = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> mouthLeft = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> browRight = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> browLeft = new ArrayList<BufferedImage>();
	double tiltAngles[];
	double shiftUps[];
	double shiftLats[];
	ArrayList<BufferedImage> bodiesR = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> bodiesL = new ArrayList<BufferedImage>();
	@SuppressWarnings("rawtypes")
	HashMap<Integer, ArrayList<ArrayList>> handsObj = new HashMap<Integer, ArrayList<ArrayList>>();

	public Puppet(String name) {
		charName = name;

		this.init();
	}

	@SuppressWarnings("rawtypes")
	private void init() {

		try {

			FileInputStream details = new FileInputStream("resources/"
					+ charName + "/details.properties");

			Properties detailsProp = new Properties();

			// loads details file
			detailsProp.load(details);

			// Saves puppet details from properties
			jumpDist = Integer.parseInt(detailsProp.getProperty("jumpDist",
					10 + ""));

			moveSpeed = Integer.parseInt(detailsProp.getProperty("moveSpeed",
					270 + ""));
			speakMax = Integer.parseInt(detailsProp.getProperty("speakMax",
					12 + ""));
			eyebrowMax = Integer.parseInt(detailsProp.getProperty("eyebrowMax",
					120 + ""));
			deadbonesStyle = Boolean.parseBoolean(detailsProp.getProperty(
					"deadbonesCharacter", "false"));
			if (deadbonesStyle) {
				tiltMax = Integer.parseInt(detailsProp.getProperty("tiltMax",
						120 + ""));
				tiltAngles = convertToNumbers(detailsProp.getProperty("tilts",
						"0,0,0,0").split(","));
				shiftUps = convertToNumbers(detailsProp.getProperty("shiftUps",
						"0,0,0,0").split(","));
				shiftLats = convertToNumbers(detailsProp.getProperty(
						"shiftHorizontal", "0,0,0,0").split(","));

			}
			JSONParser parser = new JSONParser();

			hatsRight = new ArrayList<BufferedImage>();
			hatsLeft = new ArrayList<BufferedImage>();

			try {
				Object obj = parser.parse(new FileReader("resources/"
						+ charName + "/bodies.json"));

				JSONArray bodies = (JSONArray) obj;
				for (int i = 0; i < bodies.size(); i++) {
					JSONObject body = (JSONObject) bodies.get(i);
					bodiesR.add(ImageLoader.loadImage((String) body.get("filePathBody")));

				}

				AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
				tx.translate(-bodiesR.get(0).getWidth(), 0);
				AffineTransformOp op = new AffineTransformOp(tx,
						AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

				for (int i = 0; i < bodies.size(); i++) {
					JSONObject body = (JSONObject) bodies.get(i);
					if (body.containsKey("filePathBodyLeft")) {
						bodiesL.add(ImageLoader.loadImage((String) body.get("filePathBodyLeft")));
					} else {
						bodiesL.add(op.filter(bodiesR.get(i), null));
					}

				}

				obj = parser.parse(new FileReader("resources/" + charName
						+ "/hats.json"));
				JSONArray hats = (JSONArray) obj;
				for (int i = 0; i < hats.size(); i++) {
					JSONObject hatObj = (JSONObject) hats.get(i);
					hatsRight.add(ImageLoader.loadImage((String) hatObj
							.get("filePathHat")));
					if (hatObj.containsKey("filePathHatLeft")) {
						hatsLeft.add(ImageLoader.loadImage((String) hatObj
								.get("filePathHatLeft")));
					} else {
						hatsLeft.add(op.filter(hatsRight.get(i), null));
					}
					if (hatObj.containsKey("defaultEquip")) {
						if ((Boolean) hatObj.get("defaultEquip")) {

							System.out.println("Added Default hat for "
									+ charName);
							hatsList.add(new Integer(i));

						}
					}
				}
				obj = parser.parse(new FileReader("resources/" + charName
						+ "/emotes.json"));
				JSONArray emotes = (JSONArray) obj;
				for (int i = 0; i < emotes.size(); i++) {
					JSONObject emote = (JSONObject) emotes.get(i);
					mouthRight.add(ImageLoader.loadImage((String) emote.get("filePathMouth")));
					mouthLeft.add(op.filter(mouthRight.get(i), null));
					browRight.add(ImageLoader.loadImage((String) emote.get("filePathEyebrows")));
					browLeft.add(op.filter(browRight.get(i), null));
				}
				obj = parser.parse(new FileReader("resources/" + charName
						+ "/hands.json"));
				JSONArray hands = (JSONArray) obj;
				for (int i = 0; i < hands.size(); i++) {
					JSONObject hand = (JSONObject) hands.get(i);
					if ((Boolean) hand.get("animated")) {

						String filepathhand = (String) hand.get("filePath");
						Long frameDelay = (Long) hand.get("frameDelay");
						File[] files = new File(filepathhand).listFiles();
						ArrayList<String> fileList = new ArrayList<String>();

						//if listFiles() returns null then continue the loop instead of catching the exception and
						//continuing
						if (files == null) {
							continue;
						}

						for (File file : files) {
							if (file.isFile()) {
								fileList.add(file.getName());

							}
						}
						Collections.sort(fileList, new AlphanumComparator());
						ArrayList<BufferedImage> handRight = new ArrayList<BufferedImage>();
						ArrayList<BufferedImage> handLeft = new ArrayList<BufferedImage>();
						for (String s : fileList) {
							handRight.add(ImageLoader.loadImage(filepathhand
									+ s));
							handLeft.add(op.filter(
									ImageLoader.loadImage(filepathhand + s),
									null));
						}
						ArrayList<Long> frameDelayArr = new ArrayList<Long>();
						frameDelayArr.add(frameDelay);
						ArrayList<ArrayList> handArr = new ArrayList<ArrayList>();
						handArr.add(handRight);
						handArr.add(handLeft);
						handArr.add(frameDelayArr);

						handsObj.put(new Integer(i), handArr);
					} else {
						String filepathhand = (String) hand.get("filePath");
						ArrayList<BufferedImage> handRight = new ArrayList<BufferedImage>();
						ArrayList<BufferedImage> handLeft = new ArrayList<BufferedImage>();
						handRight.add(ImageLoader.loadImage(filepathhand));
						handLeft.add(op.filter(
								ImageLoader.loadImage(filepathhand), null));

						ArrayList<ArrayList> handArr = new ArrayList<ArrayList>();
						handArr.add(handRight);
						handArr.add(handLeft);
						handsObj.put(new Integer(i), handArr);
					}

				}

			} catch (Exception e) {
				e.printStackTrace();

			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		// Return images
	}

	@SuppressWarnings("rawtypes")
	public HashMap<Integer, ArrayList<ArrayList>> getHandsObj() {
		return handsObj;
	}

	public ArrayList<BufferedImage> getBodiesR() {
		return bodiesR;
	}

	public ArrayList<BufferedImage> getBodiesL() {
		return bodiesL;
	}

	public ArrayList<BufferedImage> getMouthRight() {
		return mouthRight;
	}

	public ArrayList<BufferedImage> getMouthLeft() {
		return mouthLeft;
	}

	public ArrayList<BufferedImage> getBrowRight() {
		return browRight;
	}

	public ArrayList<BufferedImage> getBrowLeft() {
		return browLeft;
	}

	public ArrayList<BufferedImage> getHatsRight() {
		return hatsRight;
	}

	public ArrayList<BufferedImage> getHatsLeft() {
		return hatsLeft;
	}

	private double[] convertToNumbers(String[] strings) {
		double[] toReturn = new double[strings.length];
		for (int i = 0; i < strings.length; i++) {
			toReturn[i] = Double.parseDouble(strings[i]);
		}
		return toReturn;
	}
}
