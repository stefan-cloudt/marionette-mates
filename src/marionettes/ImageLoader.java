package marionettes;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageLoader {
    public static BufferedImage loadImage(String path) throws IOException {
        File file = new File(path);

        if (!file.exists()) {
            throw new IOException("File \"" + path + "\" does not exist.");
        }

        return ImageIO.read(file);
    }
}
