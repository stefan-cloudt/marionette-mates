package marionettes;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.*;
import java.io.*;

/**
 * ----------------------------------------------Marionette Mates------------------------------------------------
 * | Original Source by Bakkidza on Reddit                                                                      |
 * |- https://www.reddit.com/r/BuffaloWizards/comments/3g6f43/fuppet_fals_a_puppet_pals_knockoff_that_you_can/  |
 * | Changes and improvements by Ryuski and Endsgamer                                                           |
 * --------------------------------------------------------------------------------------------------------------
 */

@SuppressWarnings("serial")
public class Projection extends JFrame {
	private static final String LOG_FILE = "error.log";

	/**
	 * Projection Constructor - Sets up the frame.
	 */
	public Projection() {
		initializeErrorLog();

		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        setSize(new Dimension(1920,1080));
        setMinimumSize(new Dimension(1280,720));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		setTitle("Marrionette Mates");		
		ShowPanel showPanel = new ShowPanel();
		UIPanel uiPanel = showPanel.getUIPanel();
		
		showPanel.addKeyListener(showPanel);
		showPanel.addComponentListener(showPanel);
		showPanel.setFocusable(true);
		uiPanel.addMouseListener(uiPanel);
		uiPanel.addComponentListener(uiPanel);
		uiPanel.setFocusable(true);
		
		this.add(showPanel,BorderLayout.CENTER);
		this.add(uiPanel, BorderLayout.SOUTH);
		
		try {
			this.setIconImage(ImageLoader.loadImage("resources/UI/icon.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		uiPanel.setPreferredSize(new Dimension(1920,290));
		setVisible(true);
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		new Projection();
		
	}

	private static boolean initializeErrorLog() {
		FileOutputStream fos = null;
		try {
			File log = new File(LOG_FILE);
			if (log.exists()) {
				log.delete();
			}
			fos = new FileOutputStream(log);
			PrintStream ps = new PrintStream(fos);
			System.setErr(ps);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
}