package marionettes;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class CustomDie {
	
	
	BufferedImage die; //the die background
	BufferedImage[] sides = new BufferedImage[20]; //array of all the sides
	Point coords; //a tuple of coordinates, x and y
	Point2D.Double velocity; //a tuple of doubles, for more precise
							//velocity calculations
	public double gravity = 2; //arbitrary, but fine tuned
	public long startTime; //when the die started to roll
	int currentNum =1;	//what number the die should start on (keep it -1)
	int finalNum; //what the die should end up on
	boolean dramatic; //if the die should be "dramatic" or take longer, basically
	boolean rolling; //if the die is still moving/spinning
	boolean rotating;//if the die is finished rotating
	int changedCount=0; //how many times the die should change, we do it once per second
	boolean forward = true; //the die is moving towards the right
	int defaultHeight; //saved height of the die.getHeight(), since it
						//actually changes when using the rotation transform
	int defaultWidth; //same deal as default height
	public double rotationRequired = 0; //what the overall rotation should
										//be for the rotation transform
	public boolean descending = false; //if the die is going off screen
	public boolean finished=false;		//if the die should be removed
	public double nextRotation = 1.93;	//the default angle of rotation
										//per frame
	
	
	
	public CustomDie(){
		this.init();
	}
	/*
	 * reads in the images
	 */
	public void init(){
		try {
			die = ImageLoader.loadImage("resources/dice/dieBackground.png");
			defaultHeight = die.getHeight();
			defaultWidth = die.getWidth();
			for(int i = 0;i<20;i++){
				int j = i+1;
				sides[i] = ImageLoader.loadImage("resources/dice/side"+j+".png");
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
