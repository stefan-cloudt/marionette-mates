package marionettes;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class UI {
	/**
	 * Variables
	 */
	@SuppressWarnings("unused")
	private int frameHeight, frameWidth;
	private double iconScale;

	private String[] puppetNames;
	private HashMap<String, BufferedImage> puppetIcons = new HashMap<String, BufferedImage>();
	private HashMap<String, Rectangle2D> puppetIconBounds = new HashMap<String, Rectangle2D>();

	ArrayList<BufferedImage> emoteIconImages = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> hatIconImages = new ArrayList<BufferedImage>();
	private HashMap<Integer, Rectangle2D> emoteIconBounds = new HashMap<Integer, Rectangle2D>();
	private HashMap<Integer, Rectangle2D> hatIconBounds = new HashMap<Integer, Rectangle2D>();

	private Graphics2D g2;
	private int puppetX = 0;
	private int emoteNumber;
	private int hatNumber;
	private HashMap<String, User> connectedUsers;

	private BufferedImage bg, userList;
	
	ShowPanel showPanel;

	public UI(ShowPanel showPanel) {
		this.showPanel = showPanel;
	}

	public void initUI(int frameHeight, int frameWidth, Graphics2D g2, String[] puppets, int puppetNumber, String currentChar) {
		this.puppetNames = puppets;
		this.frameHeight = frameHeight;
		this.frameWidth = frameWidth;
		this.g2 = g2;
		int puppetY = 288;
		this.puppetX = 10;
		this.iconScale = (double)frameWidth/1920;
		if (this.iconScale > 1) this.iconScale = 1;
		//System.out.println(iconScale);
		/**
		 * Ui Background.
		 */
		try {
			bg = ImageLoader.loadImage("resources/UI/background.png");
			userList = ImageLoader.loadImage("resources/UI/userList.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.paintIcon(0, frameHeight - 300, bg);
		this.paintIcon(frameWidth - userList.getWidth() - 20, frameHeight - 291, userList);

		/**
		 * Saves the puppet names and icons into a hashmap.
		 */
		for (int i = 0; i < puppetNumber; i++) {
			try {
				//if(iconScale < 0.85) {
				//	puppetIcons.put(puppetNames[i], ImageIO.read(new File("resources/ui/test50x50.png")));
				//} else {
					puppetIcons.put(puppetNames[i], ImageLoader.loadImage("resources/" + puppetNames[i] + "/icon.png"));
				//}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		/**
		 * Draws the images for the puppet icons.
		 */
		for (int i = 0; i < puppetNumber; i++) {
			this.paintIcon(this.puppetX, this.frameHeight - puppetY, puppetIcons.get(puppetNames[i]).getScaledInstance((int)(75*iconScale), (int)(75*iconScale), Image.SCALE_DEFAULT));
			puppetIconBounds.put(puppetNames[i],
					new Rectangle2D.Float(this.puppetX, this.frameHeight - puppetY, (int)(75*iconScale), (int)(75*iconScale)));
			puppetY -= 80*iconScale;
			if (puppetY < 125) {
				this.puppetX += 80*iconScale;
				puppetY = 288;
			}
		}
		puppetY = 288;
		/**
		 * Saves the emote names and icons into a hashmap.
		 */
		emoteIconImages.clear();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("resources/" + currentChar + "/emotes.json"));
			JSONArray emotes = (JSONArray) obj;
			emoteNumber = emotes.size();
			for (int i = 0; i < emoteNumber; i++) {
				JSONObject emote = (JSONObject) emotes.get(i);
				emoteIconImages.add(ImageLoader.loadImage((String) emote.get("filePathIcon")));
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		hatIconImages.clear();
		try {
			Object obj = parser.parse(new FileReader("resources/" + currentChar + "/hats.json"));
			JSONArray hats = (JSONArray) obj;
			hatNumber = hats.size();
			for (int i = 0; i < hatNumber; i++) {
				JSONObject hat = (JSONObject) hats.get(i);
				//TODO hat icons
				
				hatIconImages.add(ImageLoader.loadImage("resources/"+currentChar+"/Hats/" + hat.get("hatName") + ".png"));
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		/**
		 * Draws the images for the emote icons.
		 */
		for (int i = 0; i < emoteNumber; i++) {
			this.paintIcon(this.puppetX + 100, this.frameHeight - puppetY, emoteIconImages.get(i).getScaledInstance((int)(75*iconScale), (int)(75*iconScale), Image.SCALE_DEFAULT));
			emoteIconBounds.put(i, new Rectangle2D.Float(this.puppetX + 100, this.frameHeight - puppetY, (int)(75*iconScale), (int)(75*iconScale)));
			puppetY -= 80*iconScale;
			if (puppetY < 125) {
				this.puppetX += 80*iconScale;
				puppetY = 288;
			}
		}
		puppetY = 288;
		for (int i = 0; i < hatNumber; i++) {
			this.paintIcon(this.puppetX + 200, this.frameHeight - puppetY, hatIconImages.get(i).getScaledInstance((int)(75*iconScale), (int)(75*iconScale), Image.SCALE_DEFAULT));
			hatIconBounds.put(i, new Rectangle2D.Float(this.puppetX + 200, this.frameHeight - puppetY, (int)(75*iconScale), (int)(75*iconScale)));
			puppetY -= 80*iconScale;
			if (puppetY < 125) {
				this.puppetX += 80*iconScale;
				puppetY = 288;
			}
		}
		this.connectedUsers = showPanel.getConnectedUsers();
		if (connectedUsers != null) {
//			for (User u : connectedUsers.values()) {
////				System.out.print(u.username + ", "); //TEST
//			}
		}
		puppetY = 288;
	}

	/**
	 * Paints an icon to the ui.
	 */
	private void paintIcon(int x, int y, Image image) {
		g2.drawImage(image, x, y, null);

	}

	/**
	 * Returns a hashmap of the puppet bounding rectangles for click detection.
	 */
	public HashMap<String, Rectangle2D> getPuppetBounds() {
		return puppetIconBounds;
	}
	public HashMap<Integer, Rectangle2D> getHatBounds() {
		return hatIconBounds;
	}
	/**
	 * Returns a hashmap of the emote bounding rectangles for click detection.
	 */
	public HashMap<Integer, Rectangle2D> getEmoteBounds() {
		return emoteIconBounds;
	}

	/**
	 * Returns the amount of emotes for the selected puppet.
	 */
	public int getEmoteNumber() {
		return emoteNumber;
	}
	public int getHatNumber(){
		return hatNumber;
	}
	public void updateFrame(int width) {
		this.frameWidth = width;
	}
}
