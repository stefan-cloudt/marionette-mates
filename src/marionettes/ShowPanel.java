package marionettes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ShowPanel extends JPanel implements Runnable, KeyListener,
		ComponentListener {
	private static final long serialVersionUID = 1L;

	// Starting size of screen
	public static final int WIDTH = java.awt.GraphicsEnvironment
			.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
	public static final int HEIGHT = java.awt.GraphicsEnvironment
			.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;

	/**
	 * Variables
	 */
	private static Thread thread; // So that we can use threads
	// private boolean running; // A boolean just to keep the gameloop running
	private int FPS = 60; // Number of frames per second
	private long targetTime = 1000 / FPS; // The time between frames in
											// milliseconds
	public int frameHeight; // The current height of the frame
	public int frameWidth; // The current width of the frame
	private int puppetNumber; // which index of puppet the user is using
	public Random rand; // random number generator
	private BufferedReader in; // read messages from the server
	private PrintWriter out; // send messages to the server
	private JFrame frame = new JFrame("Connection"); // jframe for server
														// connection
	private static HashMap<String, User> connectedUsers = new HashMap<String, User>(); // map
	// of
	// users

	private Properties prop = new Properties(); // read the properties file into
												// the prop object
	Graphics2D g2;
	InputStream input = null; // for properties file
	User thisClient; // the User object that this specific client
						// uses/controls
	int slotCoords[] = new int[8]; // the coordinates for the slots where the
									// puppets can move between
	UIPanel uiPanel = new UIPanel(this);
	BufferedImage userBar;

	/**
	 * Puppets
	 */
	private Puppet[] puppetList;
	private String[] puppetNames;

	private ArrayList<CustomDie> dice = new ArrayList<CustomDie>(); // array of
																	// the dice
																	// since we
																	// can spawn
																	// multiple
	private int dieNum = 0; // which position the die should roll to

	long start;
	long elapsed;
	long wait;
	private HashMap<String, String> controls = new HashMap<String, String>();
	private int port;

	/**
	 * Opens the panel, and requests focus of the computer
	 */
	public ShowPanel() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		frameHeight = HEIGHT;
		frameWidth = WIDTH;
		setFocusable(true);
		requestFocus();
		try {
			userBar = ImageLoader.loadImage("resources/UI/userBar.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prompt for and return the address of the server.
	 */
	public String getServerAddress() {
		return JOptionPane.showInputDialog(frame,
				"Enter IP Address of the Server:",
				"Welcome to Marionette Mates", JOptionPane.QUESTION_MESSAGE);
	}

	/**
	 * Prompt for and return the desired screen name.
	 */
	public String getName() {
		return JOptionPane.showInputDialog(frame, "Choose a screen name:",
				"Screen name selection", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Reads the properties and uses the information accordingly.
	 */
	public void readProperties() {
		try {
			input = new FileInputStream("resources/config.properties");

			// loads properties file
			prop.load(input);

			// Loads puppet number from properties
			String puppetNumString = prop.getProperty("PuppetNum");
			puppetNumber = Integer.parseInt(puppetNumString);

			// Loads puppet names from properties file
			String puppetNameString = prop.getProperty("PuppetNames");
			puppetNames = new String[puppetNumber];
			puppetNames = puppetNameString.split(",");

			// Loads port number from properties file
			String portString = prop.getProperty("PortNumber", "9001");
			port = Integer.parseInt(portString);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			FileInputStream input2 = new FileInputStream("controls.properties");

			// loads properties file
			Properties prop2 = new Properties();
			prop2.load(input2);

			for (Entry<Object, Object> entry : prop2.entrySet()) {
				controls.put((String) entry.getKey(), (String) entry.getValue());
			}
			// Saves puppet number from properties

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// Creates a list of puppets with however many you chose in the
		// properties.
		puppetList = new Puppet[puppetNumber];
	}

	/**
	 * Fills the list of puppets with the names from the properties file.
	 */
	public void createPuppetList() {
		for (int i = 0; i < puppetNumber; i++) {
			puppetList[i] = new Puppet(puppetNames[i]);
		}
	}

	/**
	 * when the update listener for the frame is added, it starts the thread
	 * that mostly runs the program
	 */
	public void addNotify() {

		super.addNotify();
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * Goes through each user user connected and initializes their puppets.
	 * 
	 * @param u
	 *            , the user to load in the images for
	 */
	private void initPuppet(User u) {
		for (int i = 0; i < puppetNumber; i++) {
			if (u.currentChar.equals(puppetNames[i])) {

				u.bodiesR = puppetList[i].getBodiesR();
				u.bodiesL = puppetList[i].getBodiesL();
				u.mouthRight = puppetList[i].getMouthRight();
				u.mouthLeft = puppetList[i].getMouthLeft();
				u.browRight = puppetList[i].getBrowRight();
				u.browLeft = puppetList[i].getBrowLeft();
				u.hatsRight = puppetList[i].getHatsRight();
				u.hatsLeft = puppetList[i].getHatsLeft();
				u.jumpDist = puppetList[i].jumpDist;
				u.moveSpeed = puppetList[i].moveSpeed;
				u.speakMax = puppetList[i].speakMax;
				u.eyebrowMax = puppetList[i].eyebrowMax;
				u.deadbonesStyle = puppetList[i].deadbonesStyle;
				u.tiltMax = puppetList[i].tiltMax;
				u.tiltAngles = puppetList[i].tiltAngles;
				u.shiftLats = puppetList[i].shiftLats;
				u.shiftUps = puppetList[i].shiftUps;

				if (puppetList[i].hatsList.size() > 0) {
					u.hatsList = puppetList[i].hatsList;
				} else {
					u.hatsList = new ArrayList<Integer>();
				}
				u.handsObj = puppetList[i].getHandsObj();
				u.currentBody = 0;
				u.currentHand = 0;
				u.currentHandFrame = 0;
				u.displayHand = false;
				slotCoords[0] = 0;
				slotCoords[1] = Math.round((WIDTH / 6)) * 1;
				slotCoords[2] = Math.round((WIDTH / 6)) * 2;
				slotCoords[3] = Math.round((WIDTH / 6)) * 3;
				slotCoords[4] = Math.round((WIDTH / 6)) * 4;
				slotCoords[5] = Math.round((WIDTH / 6)) * 5;
				slotCoords[6] = frameWidth + u.bodiesR.get(0).getWidth();
				slotCoords[7] = 0 - u.bodiesR.get(0).getWidth();

			}
		}

		if (u.jumpDist > slotCoords[1] - 5) {
			u.jumpDist = slotCoords[1] - 5;
		}
		System.gc();
	}

	/**
	 * Contains the logic for starting a dice roll
	 * 
	 * @param dramatic
	 *            , if the roll should be intentionally dramatic or not, at a
	 *            pivotal moment or something
	 * @param finalNum
	 *            , what the die should land on at the end, necessary for
	 *            syncing in MP
	 * @param randDramatic
	 *            , what the random number should be to fake a roll being a 1 or
	 *            a 20, again for syncing
	 * @param dieNumber
	 */
	public void rollDie(boolean dramatic, int finalNum, int randDramatic,
			int dieNumber) {

		if (dice.size() < 5) {

			CustomDie die = new CustomDie();

			die.coords = new Point(0,
					((frameHeight / 2) - die.die.getHeight() / 2) - 300);
			if (dieNumber == 0) {
				die.velocity = new Point2D.Double(12, -20);

			} else if (dieNumber == 1) {
				die.velocity = new Point2D.Double(16, -20);

			} else if (dieNumber == 2) {
				die.velocity = new Point2D.Double(20.3, -20);
			} else if (dieNumber == 3) {
				die.nextRotation = 2.8;
				die.velocity = new Point2D.Double(42.5, -20);
			} else if (dieNumber == 4) {
				die.velocity = new Point2D.Double(49, -20);
				die.nextRotation = 2.4;
			}

			die.rolling = true;
			die.rotating = true;
			die.currentNum = rand.nextInt(20) + 1;
			die.finalNum = finalNum;

			if (die.finalNum == 20 || die.finalNum == 1 || randDramatic == 4
					|| dramatic) {
				die.dramatic = true;
			}
			die.startTime = System.currentTimeMillis();
			dice.add(die);
		}
	}

	/**
	 * Handles all the messages from the server
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void runConnect() throws UnknownHostException, IOException {
		// Make connection and initialize streams

		String serverAddress = getServerAddress().trim();
		@SuppressWarnings("resource")
		Socket socket = new Socket(serverAddress, port);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		// Process all messages from server, according to the protocol.
		String tempName = null;
		while (true) {
			String line = in.readLine();
			if (line == null) {
				return;
			}
			String[] msg = line.split(" ");

			if (line.startsWith("SUBMITNAME")) {
				tempName = getName();
				out.println(tempName);
			} else if (line.startsWith("NAMEACCEPTED")) {
				thisClient = new User(tempName);

				thisClient.currentChar = puppetNames[0];
				thisClient.slotNumber = 0;

				initPuppet(thisClient);

				repaint();
				connectedUsers.put(tempName, thisClient);
				System.out.println("User added: " + tempName);
				StringWriter writer = new StringWriter();
				prop.list(new PrintWriter(writer));
				String configString = writer.getBuffer().toString();
				out.println("CONFIGSTRING " + configString);
			} else if (line.startsWith("RAW ")) {

				if (line.endsWith("connected.")) {
					String username = line.substring(4, line.indexOf(" ", 5));
					if (!username.equalsIgnoreCase(thisClient.username)) {
						User newConnected = new User(username);
						newConnected.currentChar = puppetNames[0];
						initPuppet(newConnected);
						connectedUsers.put(username, newConnected);
						System.out.println("User added: " + username);
					}

				} else if (line.endsWith("quit.")) {
					String username = line.substring(4,
							line.indexOf(" ", 5) - 1);
					System.out.println(username);

					connectedUsers.remove(username);
					System.out.println("Removing user: " + username);

				}

			} // this is what populates the users list after you connect to the
				// server
				// so everyone that is already connected shows up
			else if (line.startsWith("CONNECTEDUSERS:")) {

				out.println("User connected.");
				line = line.substring(16);

				String[] users = line.split(",");

				for (String s : users) {

					String[] usersCoords = s.split("&");
					User newUser = new User(usersCoords[0]);
					newUser.currentChar = puppetNames[Integer
							.parseInt(usersCoords[2].trim())];

					newUser.slotNumber = Integer
							.parseInt(usersCoords[1].trim());

					newUser.mouthShape = Integer.parseInt(usersCoords[3]);

					newUser.browShape = Integer.parseInt(usersCoords[4]);

					newUser.facing = Integer.parseInt(usersCoords[5].trim());
					newUser.currentBody = Integer.parseInt(usersCoords[6]
							.trim());

					initPuppet(newUser);
					if (usersCoords.length > 7) {

						String[] hatsArr = usersCoords[7].split("-");

						for (String h : hatsArr) {

							System.out.println("Hat on " + newUser.username
									+ " " + h);
							out.println("HatsArr =" + h);
							if (!h.equals("") && !h.equals("-")) {

								newUser.hatsList.add(Integer.parseInt(h));
							}
						}
					}

					if (usersCoords.length > 8) {
						newUser.displayHand = Boolean
								.parseBoolean(usersCoords[8]);
					}
					if (usersCoords.length > 9) {
						newUser.currentHand = Integer.parseInt(usersCoords[9]);
					}
					if (usersCoords.length > 10) {
						newUser.currentHandFrame = Integer
								.parseInt(usersCoords[10]);
					}
					newUser.xCoord = slotCoords[newUser.slotNumber];

					// newUser.moveLeft = true;
					connectedUsers.put(usersCoords[0], newUser);
					repaint();
				}

			} // if they change puppets
			if (msg.length > 1) {
				if (msg[1].equals("PUPPETSWITCH")
						&& !msg[0].equals(thisClient.username)) {

					connectedUsers.get(msg[0]).currentChar = puppetNames[Integer
							.parseInt(msg[2])];
					initPuppet(connectedUsers.get(msg[0]));
				} // if someone changes hats
				else if (msg[1].equals("HATADD")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).hatsList.add(Integer
							.parseInt(msg[2]));
				} else if (msg[1].equals("HATREMOVE")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).hatsList.remove(new Integer(
							Integer.parseInt(msg[2])));
				} else if (msg[1].equals("HATS")
						&& !msg[0].equals(thisClient.username)) {
					if (msg.length > 2) {
						String[] hatsToSet = msg[2].split(",");
						ArrayList<Integer> temp = new ArrayList<Integer>();
						for (String s : hatsToSet) {
							temp.add(new Integer(Integer.parseInt(s)));
						}
						connectedUsers.get(msg[0]).hatsList = temp;
					} else {
						connectedUsers.get(msg[0]).hatsList.clear();
					}
				}
				// if someone is talking
				// also controls some of the browshape and mouthshape logic
				else if (msg[1].equals("SPEAKING")
						&& !msg[0].equals(thisClient.username)) {
					if (!connectedUsers.get(msg[0]).speaking
							&& Boolean.parseBoolean(msg[2])) {

						connectedUsers.get(msg[0]).prevBrow = connectedUsers
								.get(msg[0]).browShape;
						connectedUsers.get(msg[0]).prevMouth = connectedUsers
								.get(msg[0]).mouthShape;
					} else if (connectedUsers.get(msg[0]).speaking
							&& !Boolean.parseBoolean(msg[2])) {
						connectedUsers.get(msg[0]).browShape = connectedUsers
								.get(msg[0]).prevBrow;
						connectedUsers.get(msg[0]).mouthShape = connectedUsers
								.get(msg[0]).prevMouth;
					}

					connectedUsers.get(msg[0]).speaking = Boolean
							.parseBoolean(msg[2]);
				}
				// if someone moves to the right
				else if (msg[1].equals("MOVERIGHT")
						&& !msg[0].equals(thisClient.username)) {

					boolean moved = Boolean.parseBoolean(msg[2]);
					connectedUsers.get(msg[0]).moveRight = moved;
					if (moved) {
						connectedUsers.get(msg[0]).slotNumber = (connectedUsers
								.get(msg[0]).slotNumber + 1) % 7;
					}

				} // if someone moves to the left
				else if (msg[1].equals("MOVELEFT")
						&& !msg[0].equals(thisClient.username)) {

					boolean moved = Boolean.parseBoolean(msg[2]);

					connectedUsers.get(msg[0]).moveLeft = moved;
					if (moved) {
						connectedUsers.get(msg[0]).slotNumber = (connectedUsers
								.get(msg[0]).slotNumber - 1) % 7;
						if (connectedUsers.get(msg[0]).slotNumber == -1) {
							connectedUsers.get(msg[0]).slotNumber = 7;
						}
					}
				} // if they change which direction they're facing
				else if (msg[1].equals("FACING")
						&& !msg[0].equals(thisClient.username)) {
					if (msg[2].equals("right")) {
						connectedUsers.get(msg[0]).facing = Expressions.FACING_RIGHT;
					} else if (msg[2].equals("left")) {
						connectedUsers.get(msg[0]).facing = Expressions.FACING_LEFT;
					}
				} // if they change what mouthshape they have
				else if (msg[1].equals("MOUTH")
						&& !msg[0].equals(thisClient.username)) {

					connectedUsers.get(msg[0]).mouthShape = Integer
							.parseInt(msg[2]);
				} // if they change what browshape they have
				else if (msg[1].equals("BROW")
						&& !msg[0].equals(thisClient.username)) {

					connectedUsers.get(msg[0]).browShape = Integer
							.parseInt(msg[2]);

				} // what the xcoord is, forces a sync between server and client
				else if (msg[1].equals("XCOORD")
						&& !msg[0].equals(thisClient.username)) {
					int slotNum = Integer.parseInt(msg[2]);
					connectedUsers.get(msg[0]).slotNumber = slotNum;
				} // if someone rolls a die
				else if (msg[1].equals("ROLL")) {
					dieNum = (dieNum + 1) % 5;
					rollDie(Boolean.parseBoolean(msg[2]),
							Integer.parseInt(msg[3]), Integer.parseInt(msg[4]),
							Integer.parseInt(msg[5]));
				} else if (msg[0].equals("DIENUM")) {
					dieNum = Integer.parseInt(msg[1]);
				} else if (msg[1].equals("BODY")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).currentBody = Integer
							.parseInt(msg[2]);
				} else if (msg[1].equals("ANIMATE")
						&& !msg[0].equals(thisClient.username)) {
					boolean animated = Boolean.parseBoolean(msg[2]);
					if (!animated) {
						connectedUsers.get(msg[0]).currentHandFrame = 0;
					}
					connectedUsers.get(msg[0]).animating = animated;
				} else if (msg[1].equals("DISPLAYHAND")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).displayHand = Boolean
							.parseBoolean(msg[2]);
				} else if (msg[1].equals("HAND")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).currentHand = Integer
							.parseInt(msg[2]);
				} else if (msg[1].equals("HANDFRAME")
						&& !msg[0].equals(thisClient.username)) {
					connectedUsers.get(msg[0]).currentHandFrame = Integer
							.parseInt(msg[2]);
				} else if (msg[1].equals("CONFIGMISMATCH")) {
					JOptionPane.showMessageDialog(null, msg[0]
							+ " has a different config than everyone else.");
				}

			}
		}
	}

	/**
	 * called when the thread starts that controls most of the program
	 */
	public void run() {

		// runConnect() needs to be on its own thread because it has a while
		// loop to listen
		// to the server
		new Thread(new Runnable() {
			public void run() {
				try {
					runConnect();
				} catch (UnknownHostException e1) {

					e1.printStackTrace();
					System.exit(0);
				} catch (IOException e1) {

					e1.printStackTrace();
					System.exit(0);
				}
			}
		}).start();

		this.readProperties();

		this.createPuppetList();

		this.setBackground(Color.GREEN); // Turn on the greenscreen

		// Start the timers for the loop

		// Start the random number generator
		rand = new Random();

		// Get the frame dimensions
		frameHeight = HEIGHT;
		frameWidth = WIDTH;

		// Initialize UI
		uiPanel.setPuppetNames(puppetNames);
		uiPanel.setCurrentPuppet(puppetNames[0], false);

		// Begin the gameloop

		while (true) {
			boolean dummy = runLoop();
			if (!dummy) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
		}
	}

	public boolean runLoop() {
		// if thisClient is null, it means they havent logged in, so nothing
		// should
		// be updating yet
		if (thisClient == null) {
			// System.out.println("running");
			return false;
		} else {
			// if there is at least one connected user
			if (connectedUsers.size() > 0) {
				start = System.nanoTime(); // Restart the wait for the next
											// frame
											// System.out.println(start);

				update(); // Update the locations/states of everything

				// Next frame calculations

				elapsed = System.nanoTime() - start;

				wait = targetTime - elapsed / 1000000;

				if (wait < 0) {
					wait = 5;
				}

				try {
					Thread.sleep(wait);
				} catch (Exception e) {
					e.printStackTrace();
				}

				repaint(); // Update the graphics

				return true;
			} else {
				return false;
				// System.out.println("No users in connectedUsers");
			}
		}
	}

	/**
	 * Updates the position, expression, etc of the puppets for each connected
	 * user.
	 */
	public void update() {

		// iterate through all the die in the list
		for (int i = 0; i < dice.size(); i++) {
			CustomDie die = dice.get(i);

			if (die != null) {
				if (die.rolling) {
					// if the die is "rolling", calculate the stuff for movement
					// and the stuff for changing the displayed number

					long diff = System.currentTimeMillis() - die.startTime;

					if (diff < 3000 && !die.dramatic && !die.descending) {
						if (die.coords.y + die.defaultHeight >= frameHeight - 270) {
							// Here's collision

							die.velocity.y = (-(die.velocity.y) * .73);

							die.coords.y = frameHeight - die.defaultHeight
									- 270;
							die.velocity.x = (die.velocity.x * .93);

						}
						if (die.coords.x + die.defaultWidth >= frameWidth) {
							die.velocity.x = (-(die.velocity.x) * .73);
							die.forward = false;
							die.coords.x = frameWidth - 330;

						}
						if (die.coords.x < 0) {
							die.velocity.x = (-(die.velocity.x) * .73);
							die.forward = true;
							die.coords.x = 0;

						}

						die.coords.x += die.velocity.x;
						die.coords.y += die.velocity.y;
						die.velocity.y += die.gravity;
						die.velocity.x = (die.velocity.x * .99);

						if (diff / 1000 > die.changedCount) {
							die.currentNum = rand.nextInt(20) + 1;
							die.changedCount++;
						}
					} else if (diff < 6000 && die.dramatic) {

						if (die.velocity.x < 2 && die.velocity.x > -2) {
							die.velocity.x = 0;
						}
						if (die.coords.y + die.defaultHeight >= frameHeight - 270) {
							// Here's collision

							die.velocity.y = (-(die.velocity.y) * .73);

							die.coords.y = frameHeight - die.defaultHeight
									- 270;
							die.velocity.x = (die.velocity.x * .93);

						}
						if (die.coords.x + die.defaultWidth >= frameWidth) {
							die.velocity.x = (-(die.velocity.x) * .73);
							die.forward = false;
							die.coords.x = frameWidth - 330;

						}
						if (die.coords.x < 0) {
							die.velocity.x = (-(die.velocity.x) * .73);
							die.forward = true;
							die.coords.x = 0;

						}

						// this is actually gravity
						die.coords.x += die.velocity.x;
						die.coords.y += die.velocity.y;
						die.velocity.y += die.gravity;
						die.velocity.x = (die.velocity.x * .99);

						if (diff / 1000 > die.changedCount) {
							die.currentNum = rand.nextInt(20) + 1;
							die.changedCount++;
						}
					} else if (diff >= 3000 && diff <= 5000 && !die.dramatic) {
						die.rotating = false;
						die.coords.y = frameHeight - die.die.getHeight() - 320;
						die.currentNum = die.finalNum;

					} else if (diff >= 6000 && diff <= 8000 && die.dramatic) {
						die.rotating = false;
						die.coords.y = frameHeight - die.die.getHeight() - 320;
						die.currentNum = die.finalNum;

					} else {
						die.currentNum = die.finalNum;
						die.rolling = false;
						die.descending = true;

					}
				}
				if (die.descending) {

					if (die.coords.y > frameHeight + die.defaultHeight) {
						die.finished = true;
						die.descending = false;

					} else {
						die.coords.y += 5;
					}

				}
			}
		}
		// iterate through all the connected users to update them

		for (User u : connectedUsers.values()) {

			// Update the puppet's location when moving right
			if (u.moveRight == true && u.moveLeft == false) {
				if (u.slotNumber == 0 && u.xCoord > frameWidth - 10) {
					u.xCoord = 0 - u.bodiesR.get(u.currentBody).getWidth();
				}
				if (u.xCoord < slotCoords[u.slotNumber]) {
					u.xCoord = u.xCoord + (frameWidth / u.moveSpeed);
				}
				if (u.xCoord >= slotCoords[u.slotNumber]) {
					u.moveRight = false;
				}

			}

			// Update the puppet's location when moving left
			if (u.moveLeft == true && u.moveRight == false) {

				if (u.slotNumber == 7) {

					if (u.xCoord > -274
							&& u.xCoord < frameWidth
									+ u.bodiesR.get(u.currentBody).getWidth()) {

						u.xCoord = u.xCoord - (frameWidth / u.moveSpeed);
					} else {
						u.slotNumber = 6;
					}

				} else {
					if (u.xCoord > slotCoords[u.slotNumber]) {
						u.xCoord = u.xCoord - (frameWidth / u.moveSpeed);
					}
					if (u.xCoord <= slotCoords[u.slotNumber]) {
						u.moveLeft = false;
					}
				}
			}

			// Make the puppet move up and down
			if (u.moveLeft || u.moveRight) {
				u.bounce++;
			} else if (u.bounce != 0 && u.bounce < u.jumpDist) {
				// This section is for if the move buttons stop being pressed
				// while the puppet is in the air. It just makes the puppet
				// return to the ground
				if (u.bounce < u.jumpDist / 2) {
					u.bounce = u.jumpDist - u.bounce;
				} else {
					u.bounce++;
				}
			}

			// This restarts the puppet's bounce
			if (u.bounce == u.jumpDist) {
				u.bounce = 0;
			}

			/**
			 * Some math to make the puppet's bounce more realistic, instead of
			 * just moving linearly up and down.
			 */
			u.yHeight = Math.pow((u.bounce - u.jumpDist / 2), 2)
					/ (u.jumpDist / 4);

			/**
			 * Making the y coordinate fits along the bottom of the frame
			 * correctly.
			 */
			u.yCoord = frameHeight - u.bodiesR.get(u.currentBody).getHeight()
					- 310 + (int) u.yHeight;

			/**
			 * This lets the puppet roll over when it goes off-screen to the
			 * right.
			 */
			if (u.xCoord > frameWidth + u.bodiesR.get(u.currentBody).getWidth()) {
				u.xCoord = 0 - u.bodiesR.get(u.currentBody).getWidth();
			}

			/**
			 * This lets the puppet roll over when it goes off-screen to the
			 * left.
			 */
			if (u.xCoord < 0 - u.bodiesR.get(u.currentBody).getWidth()) {
				u.xCoord = frameWidth + u.bodiesR.get(u.currentBody).getWidth();
			}

			if (u.animating && u.displayHand) {

				if (u.handFrameDelay >= (Long) u.handsObj.get(u.currentHand)
						.get(2).get(0)) {
					u.currentHandFrame = (u.currentHandFrame + 1)
							% u.handsObj.get(u.currentHand).get(0).size();
					if (u.currentHandFrame == 0) {
						u.currentHandFrame = 1;
					}
					u.handFrameDelay = 0;
				} else {
					u.handFrameDelay++;
				}
			}
			// This checks if the puppet is currently talking
			if (u.speaking) {
				u.speakCount++; // If the puppet is talking, we add one to
								// speakcount
				u.eyebrowCount++; // and one to eyebrow count

				u.tiltCount++;

				// this is similar to the mouthshape randomness when speaking
				if ((u.eyebrowCount >= u.eyebrowRand) && u.prevBrow == 0) {

					int newBrowSize = u.browRight.size();
					if (newBrowSize > 4) {
						newBrowSize = 5;
					}
					u.eyebrowNum = rand.nextInt(newBrowSize);

					u.browShape = u.eyebrowNum;
					u.eyebrowCount = 0;
					u.eyebrowRand = rand.nextInt(u.eyebrowMax) + 10;
				}
				if (u.speakCount >= u.speakRand) { // If the puppet has been
													// speaking one particular
													// sound long enough, this
													// triggers.
					int newMouthSize = u.mouthRight.size();
					if (newMouthSize > 4) {
						newMouthSize = 5;
					}
					u.mouthNum = rand.nextInt(newMouthSize); // We get a random
																// number
					u.mouthShape = u.mouthNum;
					// That random number relates to the puppet's next mouth
					// shape

					u.speakCount = 0; // We reset how long the puppet's been
										// saying this sound
					u.speakRand = rand.nextInt(u.speakMax) + 3; // We get a new
																// number to see
																// how long the
																// puppet should
																// be saying
																// this new
																// sound
				}
				if (u.deadbonesStyle && (u.tiltCount >= u.tiltRand)) { // if the
																		// character
																		// should
																		// tilt
																		// their
																		// head
																		// when
																		// they
																		// talk
					int tiltRandInt = rand.nextInt(4);
					// tilt and move their head randomly
					u.tiltAngle = u.tiltAngles[tiltRandInt];
					u.tiltShiftLat = (int) u.shiftLats[tiltRandInt];
					u.tiltShiftVert = (int) u.shiftUps[tiltRandInt];
					u.tiltCount = 0;
					u.tiltRand = rand.nextInt(u.tiltMax) + 3;
				}

			}

		}
	}

	/**
	 * Paints the images.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g2 = (Graphics2D) g;

		// iterate through the users again to draw them all

		for (User u : connectedUsers.values()) {

			if (u.bodiesR.get(u.currentBody) != null) { // Basic check to make
														// sure things are
				// still ok
				if (u.facing == Expressions.FACING_LEFT) { // Check to see
															// direction the
															// puppet is facing

					g2.drawImage(u.bodiesL.get(u.currentBody), u.xCoord,
							u.yCoord, null); // Draw
					// the
					// right
					// puppet
					// in
					// the
					// right
					// location

					if (u.deadbonesStyle) {
						// create a transformation to rotate
						// really only does something if it's a deadbones-style
						// character
						AffineTransform tx = AffineTransform.getRotateInstance(
								Math.toRadians(-u.tiltAngle),
								u.bodiesR.get(u.currentBody).getWidth() / 2,
								u.bodiesR.get(u.currentBody).getHeight() / 2);

						AffineTransformOp op = new AffineTransformOp(tx,
								AffineTransformOp.TYPE_BILINEAR);

						// This set of else-ifs draws the right mouth in the
						// right
						// location

						g2.drawImage(
								op.filter(u.mouthLeft.get(u.mouthShape), null),
								u.xCoord - u.tiltShiftLat, u.yCoord
										+ u.tiltShiftVert, null);

						/**
						 * This set of else-ifs draws the right eyebrows in the
						 * right location.
						 */
						g2.drawImage(
								op.filter(u.browLeft.get(u.browShape), null),
								u.xCoord - u.tiltShiftLat, u.yCoord
										+ u.tiltShiftVert, null);

						// draw hats in the right location

						if (u.hatsList.size() > 0 && u.hatsRight.size() > 0) {
							for (int i = 0; i < u.hatsList.size(); i++) {
								g2.drawImage(
										op.filter(u.hatsLeft.get(u.hatsList
												.get(i)), null), u.xCoord
												- u.tiltShiftLat, u.yCoord
												+ u.tiltShiftVert, null);
							}
						}
						if (u.displayHand) {
							g2.drawImage(
									(BufferedImage) u.handsObj
											.get(u.currentHand).get(1)
											.get(u.currentHandFrame), u.xCoord,
									u.yCoord, null);
						}

					} else {
						// This set of else-ifs draws the right mouth in the
						// right
						// location

						/**
						 * This set of else-ifs draws the right eyebrows in the
						 * right location.
						 */
						g2.drawImage(u.browLeft.get(u.browShape), u.xCoord,
								u.yCoord, null);

						if (u.hatsList.size() > 0 && u.hatsRight.size() > 0) {
							for (int i = 0; i < u.hatsList.size(); i++) {
								g2.drawImage(u.hatsLeft.get(u.hatsList.get(i)),
										u.xCoord, u.yCoord, null);
							}
						}
						g2.drawImage(u.mouthLeft.get(u.mouthShape), u.xCoord,
								u.yCoord, null);
						if (u.displayHand && u.handsObj.get(u.currentHand).size() >= 2) {
							g2.drawImage(
									(BufferedImage) u.handsObj
											.get(u.currentHand).get(1)
											.get(u.currentHandFrame), u.xCoord,
									u.yCoord, null);
						}

					}
				} else { // If the puppet isn't facing left, it's facing right

					/**
					 * Draw the right puppet in the right location.
					 */
					g2.drawImage(u.bodiesR.get(u.currentBody), u.xCoord,
							u.yCoord, null);
					if (u.deadbonesStyle) {
						// same deal, creating the transformation
						AffineTransform tx = AffineTransform.getRotateInstance(
								Math.toRadians(u.tiltAngle),
								u.bodiesR.get(u.currentBody).getWidth() / 2,
								u.bodiesR.get(u.currentBody).getHeight() / 2);

						AffineTransformOp op = new AffineTransformOp(tx,
								AffineTransformOp.TYPE_BILINEAR);
						// Draw the right mouth in the right location
						g2.drawImage(
								op.filter(u.mouthRight.get(u.mouthShape), null),
								u.xCoord + u.tiltShiftLat, u.yCoord
										+ u.tiltShiftVert, null);

						/**
						 * Draw the right eyebrows in the right location.
						 */

						g2.drawImage(
								op.filter(u.browRight.get(u.browShape), null),
								u.xCoord + u.tiltShiftLat, u.yCoord
										+ u.tiltShiftVert, null);

						if (u.hatsList.size() > 0 && u.hatsRight.size() > 0) {
							for (int i = 0; i < u.hatsList.size(); i++) {
								g2.drawImage(op.filter(
										u.hatsRight.get(u.hatsList.get(i)),
										null), u.xCoord + u.tiltShiftLat,
										u.yCoord + u.tiltShiftVert, null);
							}
						}
						if (u.displayHand) {
							g2.drawImage(
									(BufferedImage) u.handsObj
											.get(u.currentHand).get(0)
											.get(u.currentHandFrame), u.xCoord,
									u.yCoord, null);
						}

					} else {

						/**
						 * Draw the right eyebrows in the right location.
						 */
						g2.drawImage(u.browRight.get(u.browShape), u.xCoord,
								u.yCoord, null);

						if (u.hatsList.size() > 0 && u.hatsRight.size() > 0) {
							for (int i = 0; i < u.hatsList.size(); i++) {
								int hatId = u.hatsList.get(i);
								if (hatId >= 0 && hatId < u.hatsRight.size()) {
									g2.drawImage(
											u.hatsRight.get(u.hatsList.get(i)),
											u.xCoord, u.yCoord, null);
								}
							}
						}
						g2.drawImage(u.mouthRight.get(u.mouthShape), u.xCoord,
								u.yCoord, null);
						if (u.displayHand) {
							g2.drawImage(
									(BufferedImage) u.handsObj
											.get(u.currentHand).get(0)
											.get(u.currentHandFrame), u.xCoord,
									u.yCoord, null);
						}

					}
				}
			}
		}
		// iterate through all the dice to update them
		// if you use a foreach loop, you get concurrentmodificationexception
		for (int i = 0; i < dice.size(); i++) {
			CustomDie die = dice.get(i);
			// if the die is calculating it's numbers, and spinning
			// it should rotate around its center
			if (die != null && die.rolling && die.rotating) {

				// if it is moving to the right
				if (die.forward) {
					die.rotationRequired += Math
							.toRadians(die.nextRotation * 4);
				} else {
					die.rotationRequired -= Math
							.toRadians(die.nextRotation * 4);
				}
				// the actual rotation
				AffineTransform tx = AffineTransform.getRotateInstance(
						die.rotationRequired, die.die.getWidth() / 2,
						die.die.getHeight() / 2);

				AffineTransformOp op = new AffineTransformOp(tx,
						AffineTransformOp.TYPE_BILINEAR);

				g2.drawImage(op.filter(die.die, null), die.coords.x,
						die.coords.y, null);
				int temp = die.currentNum - 1;
				g2.drawImage(op.filter(die.sides[temp], null), die.coords.x,
						die.coords.y, null);
			} // if it's stopped moving, keep it on screen for a while so
				// you can see the result
			else if (die != null && die.rolling) {
				die.rotationRequired = 0;
				g2.drawImage(die.die, die.coords.x, die.coords.y, null);
				g2.drawImage(die.sides[die.currentNum - 1], die.coords.x,
						die.coords.y, null);
			} // makes the die slowly go offscreen
			else if (die != null && die.descending) {
				die.rotationRequired = 0;
				g2.drawImage(die.die, die.coords.x, die.coords.y, null);
				g2.drawImage(die.sides[die.currentNum - 1], die.coords.x,
						die.coords.y, null);
			} // if it's finished moving off screen
			else if (die != null && die.finished) {

				dice.remove(die);
				System.gc();

			}
		}

	}

	/**
	 * Checks for pressed keys and does the appropriate actions.
	 */
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode(); // See what the user is doing
		String key = KeyEvent.getKeyText(keyCode);

		// pressing numpad keys or number keys change what puppet you're using

		if (controls.get("puppet1").equals(key)) {
			setCurrentChar(puppetNames[0], 0);

		}
		if (controls.get("puppet2").equals(key) && puppetNumber > 1) {
			setCurrentChar(puppetNames[1], 1);

		}
		if (controls.get("puppet3").equals(key) && puppetNumber > 2) {
			setCurrentChar(puppetNames[2], 2);

		}
		if (controls.get("puppet4").equals(key) && puppetNumber > 3) {
			setCurrentChar(puppetNames[3], 3);
		}
		if (controls.get("puppet5").equals(key) && puppetNumber > 4) {
			setCurrentChar(puppetNames[4], 4);
		}
		if (controls.get("puppet6").equals(key) && puppetNumber > 5) {
			setCurrentChar(puppetNames[5], 5);
		}
		if (controls.get("puppet7").equals(key) && puppetNumber > 6) {
			setCurrentChar(puppetNames[6], 6);
		}
		if (controls.get("puppet8").equals(key) && puppetNumber > 7) {
			setCurrentChar(puppetNames[7], 7);
		}
		if (controls.get("puppet9").equals(key) && puppetNumber > 8) {
			setCurrentChar(puppetNames[8], 8);
		}
		if (controls.get("puppet10").equals(key) && puppetNumber > 9) {
			setCurrentChar(puppetNames[9], 9);
		}

		// R just turns the puppet right
		if (controls.get("turnRight").equals(key)) {
			thisClient.facing = Expressions.FACING_RIGHT;
			out.println("FACING right");
		}

		// W just turns the puppet left
		if (controls.get("turnLeft").equals(key)) {
			thisClient.facing = Expressions.FACING_LEFT;
			out.println("FACING left");
		}

		// G and Up switch the direction the puppet is facing
		if (controls.get("switchDirection").equals(key)
				|| controls.get("switchDirection2").equals(key)) {
			if (thisClient.facing == Expressions.FACING_RIGHT) {
				thisClient.facing = Expressions.FACING_LEFT;
				out.println("FACING left");
			} else {
				thisClient.facing = Expressions.FACING_RIGHT;
				out.println("FACING right");
			}
		}

		// Space starts the puppet talking
		if (controls.get("talk").equals(key)) {
			if (!thisClient.speaking) {
				thisClient.prevBrow = thisClient.browShape;
				thisClient.prevMouth = thisClient.mouthShape;
				thisClient.speaking = true;
				out.println("SPEAKING true");
			}
			// thisClient.speaking = true;
			// out.println("SPEAKING true");
		}

		// Y changes the puppet mouth to 'closed'
		if (controls.get("mouth1").equals(key)) {
			thisClient.mouthShape = 0;

			out.println("MOUTH " + thisClient.mouthShape);
		}

		// U changes the puppet mouth to 'A', pressing U while the puppet is
		// already saying 'A' will change the mouth to 'closed'
		if (controls.get("mouth2").equals(key)) {
			if (thisClient.mouthShape != 1) {
				thisClient.mouthShape = 1;

			} else {
				thisClient.mouthShape = 0;

			}
			out.println("MOUTH " + thisClient.mouthShape);
		}

		// I changes the puppet mouth to 'I', double tap resets to 'closed'
		if (controls.get("mouth3").equals(key)) {
			if (thisClient.mouthShape != 2) {
				thisClient.mouthShape = 2;

			} else {
				thisClient.mouthShape = 0;
			}
			out.println("MOUTH " + thisClient.mouthShape);
		}

		// O changes the puppet mouth to 'O', double tap resets to 'closed'
		if (controls.get("mouth4").equals(key)) {
			if (thisClient.mouthShape != 3) {
				thisClient.mouthShape = 3;
			} else {
				thisClient.mouthShape = 0;
			}
			out.println("MOUTH " + thisClient.mouthShape);
		}

		// P changes the puppet mouth to 'E', double tap resets to 'closed'
		if (controls.get("mouth5").equals(key)) {
			if (thisClient.mouthShape != 4) {
				thisClient.mouthShape = 4;
			} else {
				thisClient.mouthShape = 0;
			}
			out.println("MOUTH " + thisClient.mouthShape);
		}

		// H changes the puppet eyebrows to 'normal' (no particular emotion)
		if (controls.get("brow1").equals(key)) {
			thisClient.browShape = 0;
			out.println("BROW " + thisClient.browShape);
		}

		// J changes the puppet eyebrows to 'excited', double tap resets to
		// 'normal'
		if (controls.get("brow2").equals(key)) {
			if (thisClient.browShape != 1) {
				thisClient.browShape = 1;
			} else {
				thisClient.browShape = 0;
			}
			out.println("BROW " + thisClient.browShape);
		}

		// K changes the puppet eyebrows to 'angry', double tap resets to
		// 'normal'
		if (controls.get("brow3").equals(key)) {
			if (thisClient.browShape != 2) {
				thisClient.browShape = 2;
			} else {
				thisClient.browShape = 0;
			}
			out.println("BROW " + thisClient.browShape);
		}

		// L changes the puppet eyebrows to 'sad', double tap resets to 'normal'
		if (controls.get("brow4").equals(key)) {
			if (thisClient.browShape != 3) {
				thisClient.browShape = 3;
			} else {
				thisClient.browShape = 0;
			}
			out.println("BROW " + thisClient.browShape);
		}

		// Semicolon changes the puppet eyebrows to 'confused', double tap
		// resets to 'normal'
		if (controls.get("brow5").equals(key)) {
			if (thisClient.browShape != 4) {
				thisClient.browShape = 4;
			} else {
				thisClient.browShape = 0;
			}
			out.println("BROW " + thisClient.browShape);
		}

		// Escape closes the program
		if (controls.get("quit").equals(key)) {
			System.exit(0);
		}
		if (controls.get("animateHand").equals(key)
				&& thisClient.handsObj.get(thisClient.currentHand).get(0)
						.size() > 1) {
			if (!thisClient.animating) {
				thisClient.prevDisplayHand = thisClient.displayHand;
				thisClient.displayHand = true;
				thisClient.animating = true;
				out.println("ANIMATE true");
			}

		}
		if (controls.get("nextHandFrame").equals(key)) {
			thisClient.currentHandFrame = (thisClient.currentHandFrame + 1)
					% thisClient.handsObj
							.get(new Integer(thisClient.currentHand)).get(0)
							.size();
			out.println("HANDFRAME " + thisClient.currentHandFrame);
		}
		if (controls.get("nextHand").equals(key)) {
			thisClient.currentHandFrame = 0;
			thisClient.currentHand = (thisClient.currentHand + 1)
					% thisClient.handsObj.size();
			out.println("HAND " + thisClient.currentHand);
		}

		if (controls.get("displayHand").equals(key)) {
			if (thisClient.displayHand) {
				thisClient.displayHand = false;
			} else {
				thisClient.displayHand = true;
			}
			out.println("DISPLAYHAND " + thisClient.displayHand);
		}

	}

	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode(); // What the user has stopped doing
		String key = KeyEvent.getKeyText(keyCode);

		// If the user releases either F or Right Arrow, we stop moving right
		if (controls.get("moveRight").equals(key)
				|| controls.get("moveRight2").equals(key)) {

			if (thisClient.facing == Expressions.FACING_RIGHT) {
				thisClient.slotNumber = (thisClient.slotNumber + 1) % 7;
				out.println("MOVERIGHT true");
				out.println("MOVELEFT false");
				out.println("FACING right");
				out.println("XCOORD " + thisClient.slotNumber);
				thisClient.facing = Expressions.FACING_RIGHT;
				thisClient.moveRight = true;
				thisClient.moveLeft = false;
			} else {
				out.println("FACING right");
				thisClient.facing = Expressions.FACING_RIGHT;
			}
		}

		// If the user releases either S or Left Arrow, we stop moving left
		if (controls.get("moveLeft").equals(key)
				|| controls.get("moveLeft2").equals(key)) {

			if (thisClient.facing == Expressions.FACING_LEFT) {

				thisClient.slotNumber = (thisClient.slotNumber - 1) % 7;

				if (thisClient.slotNumber == -1) {
					thisClient.slotNumber = 7;
				}

				out.println("MOVELEFT true");
				out.println("MOVERIGHT false");
				out.println("FACING left");
				out.println("XCOORD " + thisClient.slotNumber);
				thisClient.facing = Expressions.FACING_LEFT;
				thisClient.moveLeft = true;
				thisClient.moveRight = false;
			} else {
				out.println("FACING left");
				thisClient.facing = Expressions.FACING_LEFT;
			}
		}

		// If the user releases Space, we stop talking, reset the speak counter,
		// and close the mouth
		if (controls.get("talk").equals(key)) {
			thisClient.speaking = false;
			thisClient.speakCount = 0;
			thisClient.mouthShape = thisClient.prevMouth;
			thisClient.browShape = thisClient.prevBrow;

			out.println("SPEAKING false");
			thisClient.tiltAngle = 0;
			thisClient.tiltShiftVert = 0;
			thisClient.tiltShiftLat = 0;
			out.println("BROW " + thisClient.browShape);
			out.println("MOUTH " + thisClient.mouthShape);

		}
		// rolls a random d20, potentially a dramatic one
		if (controls.get("roll").equals(key)) {

			if (dice.size() < 6) {
				int finalNum = rand.nextInt(20) + 1;
				int randDramatic = rand.nextInt(20) + 1;

				dieNum = (dieNum + 1) % 5;
				// rollDie(false, finalNum, randDramatic);
				out.println("ROLL false " + finalNum + " " + randDramatic);
			}
		}
		// rolls a random d20, forcing a dramtic roll
		if (controls.get("dramaticRoll").equals(key)) {
			if (dice.size() < 6) {
				int finalNum = rand.nextInt(20) + 1;
				int randDramatic = rand.nextInt(20) + 1;
				dieNum = (dieNum + 1) % 5;
				// rollDie(true, finalNum, randDramatic);
				out.println("ROLL true " + finalNum + " " + randDramatic);
			}
		}

		if (controls.get("cycleBody").equals(key)) {

			thisClient.currentBody = (thisClient.currentBody + 1)
					% (thisClient.bodiesR.size());
			out.println("BODY " + thisClient.currentBody);

		}
		if (keyCode == KeyEvent.VK_COMMA) {
			// thisClient.currentBody=thisClient.currentBody+1%thisClient.bodiesR.size();

		}
		if (controls.get("animateHand").equals(key)
				&& thisClient.handsObj.get(thisClient.currentHand).get(0)
						.size() > 1) {

			thisClient.displayHand = thisClient.prevDisplayHand;
			thisClient.animating = false;

			thisClient.currentHandFrame = 0;
			out.println("ANIMATE false");

		}
	}

	/*
	 * updates stuff when the window is resized (non-Javadoc)
	 * 
	 * @see java.awt.event.ComponentListener#componentResized(java.awt.event.
	 * ComponentEvent)
	 */
	public void componentResized(ComponentEvent arg0) { // Automatically resizes
														// where the puppet goes
														// when the window is
														// resized
		JFrame frameTemp = (JFrame) SwingUtilities.getWindowAncestor(this);
		frameWidth = frameTemp.getWidth();
		frameHeight = frameTemp.getHeight() - 16;
		if (thisClient != null) {
			slotCoords[0] = 0;
			slotCoords[1] = Math.round((frameWidth / 6)) * 1;
			slotCoords[2] = Math.round((frameWidth / 6)) * 2;
			slotCoords[3] = Math.round((frameWidth / 6)) * 3;
			slotCoords[4] = Math.round((frameWidth / 6)) * 4;
			slotCoords[5] = Math.round((frameWidth / 6)) * 5;
			slotCoords[6] = frameWidth + thisClient.bodiesR.get(0).getWidth();
			slotCoords[7] = 0 - thisClient.bodiesR.get(0).getWidth();

			for (User u : connectedUsers.values()) {
				if (!u.moveLeft && !u.moveRight) {
					u.xCoord = slotCoords[u.slotNumber];
				}
			}
		}
	}

	// these are unused because we dont need them, they are here because
	// they're inherited from keylistener and componentlistner
	public void keyTyped(KeyEvent e) {

	}

	public void componentHidden(ComponentEvent arg0) {

	}

	public void componentMoved(ComponentEvent arg0) {

	}

	public void componentShown(ComponentEvent arg0) {

	}

	/**
	 * Returns the array of Puppet Names.
	 */
	public String[] getPuppetNames() {
		return puppetNames;
	}

	/**
	 * Sets the current character.
	 */
	public void setCurrentChar(String name, int puppetNumber) {
		if (thisClient != null) {
			if (!thisClient.currentChar.equals(name)) {
				thisClient.currentBody = 0;
				thisClient.currentHand = 0;
				thisClient.currentHandFrame = 0;
				thisClient.displayHand = false;
				thisClient.currentChar = name;

				initPuppet(thisClient);

				ArrayList<Integer> tempArr = new ArrayList<Integer>();
				for (int i : thisClient.hatsList) {
					tempArr.add(new Integer(i));

				}
				this.setCurrentHats(tempArr);
				uiPanel.setCurrentPuppet(name, true);

				uiPanel.hats = tempArr;
				System.out.println(puppetNumber);
				out.println("PUPPETSWITCH " + puppetNumber);
			}
		}
	}

	/**
	 * Sets the current emote
	 */
	public void setCurrentEmote(int emote) {
		if (thisClient != null) {
			thisClient.mouthShape = emote;
			thisClient.prevMouth = emote;
			thisClient.browShape = emote;
			thisClient.prevBrow = emote;
			thisClient.emoteNum = emote;
			out.println("MOUTH " + thisClient.mouthShape);
			out.println("BROW " + thisClient.browShape);
		}
	}

	public void setCurrentHats(ArrayList<Integer> newHats) {

		thisClient.hatsList = new ArrayList<Integer>();

		String hatsToSend = "";
		for (int i = 0; i < newHats.size(); i++) {
			thisClient.hatsList.add(newHats.get(i));
			hatsToSend += newHats.get(i) + ",";
		}
		out.println("HATS " + hatsToSend);
	}

	public UIPanel getUIPanel() {
		return uiPanel;
	}

	public int indexOf(String[] array, String value) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == value)
				return i;
		return -1;
	}

	public HashMap<String, User> getConnectedUsers() {
		return connectedUsers;
	}
}
